import pandas as pd
import numpy as np
import datetime as dt
BITCOIN_PATH = "data/all_both_cryptocurrency.csv"
prices_dataframe = pd.read_csv(BITCOIN_PATH, parse_dates=["date"])
prices_dataframe["date"] = prices_dataframe.date.dt.date
prices_dataframe.sort_values(by=["date", "hour"], ascending=True, inplace=True)
datetime_fun = lambda d_h: dt.datetime(d_h[0].year, d_h[0].month, d_h[0].day, d_h[1])
time_series = np.apply_along_axis(datetime_fun, 1, prices_dataframe[["date","hour"]].values) 


TWEETS_PATH = "data/twitter_data/bitcoin_tweets_sentiment.csv"
tweets_dataframe = pd.read_csv(TWEETS_PATH, parse_dates=["date"])
tweets_dataframe["date"] = tweets_dataframe.date.dt.date
tweets_dataframe.sort_values(by=["date", "hour"], ascending=True, inplace=True)
time_series_2 = np.apply_along_axis(datetime_fun, 1, tweets_dataframe[["date","hour"]].values) 

columns_bitcoin = ["close_btc", "high_btc", "low_btc", "volume_btc", "volume_btc_usd"]
columns_etherium = ["close_eth", "high_eth", "low_eth", "volume_eth", "volume_eth_usd"]
columns_twitter = ['n_ratio_tweets', 'p_ratio_tweets', 'nl_ratio_tweets',
				 'pr_no_neutral_tweets', 'avg_polarity', 'num_tweets']

target = 'close_btc'

all_bitcoin_data_fields = columns_bitcoin + columns_etherium + columns_twitter

def get_bitcoin_data():
	return time_series, prices_dataframe[columns_bitcoin].values

def get_etherium_data():
	return time_series, prices_dataframe[columns_etherium].values

def get_twitter_data():
	return time_series_2, tweets_dataframe[columns_twitter].values

def get_all_bitcoin_data():
	full_dataframe = prices_dataframe.merge(
				tweets_dataframe, how="inner", on=["date", "hour"]
			).sort_values(by=["date", "hour"], ascending=True)[
				['date', 'hour'] + columns_bitcoin + columns_etherium + columns_twitter
			].copy()
	full_dataframe.sort_values(by=["date", "hour"], ascending=True, inplace=True)
	time_series_full = np.apply_along_axis(datetime_fun, 1, full_dataframe[["date","hour"]].values) 
	return time_series_full, full_dataframe[
									all_bitcoin_data_fields
								].values, all_bitcoin_data_fields

