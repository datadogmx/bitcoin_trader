from tensorflow.keras.models import load_model
from itertools import product
from sklearn.model_selection import KFold
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from tensorflow.keras import Sequential
from tensorflow.keras.layers import LSTM, Dense, Dropout, Activation
import time
import pandas as pd
import numpy as np

def get_arquitecture_lstm(hidden_layers, units , dataset_shape, url_save=None, patience_ear=30, **kwargs):
    mcp_save = ModelCheckpoint(url_save, save_best_only=True, monitor='val_loss', mode='min')
    earlyStopping = EarlyStopping(monitor='val_loss', patience=patience_ear, verbose=0, mode='min')
    reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=7, verbose=0, mode='min')
    callbacks = [earlyStopping, mcp_save, reduce_lr_loss]
    model = Sequential()
    for inx in range(hidden_layers):
        model.add(LSTM(units[inx],input_shape=(dataset_shape[1],dataset_shape[2]),
                    return_sequences = (False if inx == hidden_layers-1 else True)))
        model.add(Dropout(0.2))
    model.add(Dense(1, activation="relu"))
    model.compile(loss = 'mse', optimizer = 'rmsprop', metrics=['mape'] )
    return model, callbacks

def get_arquitecture_lstm_cv(hidden_layers, units , dataset_shape, patience_ear=30, **kwargs):
    earlyStopping = EarlyStopping(monitor='val_loss', patience=patience_ear, verbose=0, mode='min')
    reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=7, verbose=0, mode='min')
    callbacks = [earlyStopping, reduce_lr_loss]
    model = Sequential()
    for inx in range(hidden_layers):
        model.add(LSTM(units[inx],input_shape=(dataset_shape[1],dataset_shape[2]),
                    return_sequences = (False if inx == hidden_layers-1 else True)))
        model.add(Dropout(0.2))
    model.add(Dense(1, activation="relu"))
    model.compile(loss = 'mse', optimizer = 'rmsprop', metrics=['mape'] )
    return model, callbacks

def cross_validation_models(dataset_x, dataset_y, epochs=100, batch_size=32, hidden_layers = [1,2,3], units = [16,32,64, 128], url_to_save = None):
    start = time.time()

    configurations_dict = []
    for hd in hidden_layers:
        name = "hidden_layers_{}_".format(hd)
        posibilities = list(product(*[units for _ in range(hd)]))
        for pos in posibilities:
            configurations_dict.append({
                "name": name+"units_"+('_'.join(map(str, pos))),
                "hidden_layers":hd,
                "units":pos,
                "scores":[]
                
            })
    counter = 1
    for train_index,test_index in KFold(5, random_state=1234).split(dataset_x):
        print("Crossvalidation fold: {}".format(counter))
        for conf in configurations_dict:
            model, clls = get_arquitecture_lstm_cv(**conf, dataset_shape=dataset_x.shape)
            model.fit(dataset_x[train_index],dataset_y[train_index], validation_data=(dataset_x[test_index], dataset_y[test_index]), batch_size=batch_size, epochs=epochs, verbose=0, callbacks = clls)
            conf['scores'].append(model.evaluate(dataset_x[test_index], dataset_y[test_index], verbose = 0)[0])
        counter+=1
    for conf in configurations_dict:
        conf['avg'] = float(np.average(conf['scores']))
        conf['std'] = float(np.std(conf['scores']))
    print("Fin de crossvalidation")
    
    ord_configurations = sorted(configurations_dict, key=lambda el: el['avg'])
    data  = []
    for conf in ord_configurations:
        for score in conf['scores']:
            row = {}
            row['name'] = 'lstm_'+conf['name']
            row['hidden_layers'] = conf['hidden_layers']
            row['units'] = conf['units']
            row['score'] = score
            row['avg'] = conf['avg']
            row['std'] = conf['std']
            data.append(row)

    pd.DataFrame(data).to_csv(url_to_save, index=False)
    print("resultado guardado en:"+url_to_save)
    print ('time: {}'.format(str(time.time() - start)))