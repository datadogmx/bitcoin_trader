from tensorflow.keras.models import load_model
from itertools import product
from sklearn.model_selection import KFold
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from tensorflow.keras import Sequential
from tensorflow.keras.layers import LSTM, Dense, Dropout, Activation
import time
import pandas as pd
import numpy as np
from utils.models.dqn.dqn_wrapper import DQNWrapper

def get_arquitecture_dqn_cv(hidden_layers, units , dataset_shape, patience_ear=30, **kwargs):
    model = Sequential()
    counter = 0
    for inx in range(hidden_layers):
        if counter == 0:
            model.add(Dense(units[inx],input_shape=(dataset_shape[1],)))
            counter+=1
        else:
            model.add(Dense(units[inx]))

        model.add(Dropout(0.2))
    model.add(Dense(11, activation="relu"))
    model.compile(loss = 'mse', optimizer = 'rmsprop', verbose=0)
    return DQNWrapper(model, 0.99), None

def cross_validation_models(dataset_x, dataset_y, dataset_z, epochs=400, batch_size=32, hidden_layers = [1,2,3], units = [32, 64,128,256], url_to_save = None):
    start = time.time()

    configurations_dict = []
    for hd in hidden_layers:
        name = "hidden_layers_{}_".format(hd)
        posibilities = list(product(*[units for _ in range(hd)]))
        for pos in posibilities:
            configurations_dict.append({
                "name": name+"units_"+('_'.join(map(str, pos))),
                "hidden_layers":hd,
                "units":pos,
                "scores":[]
                
            })
    counter = 1
    for train_index,test_index in KFold(5, random_state=1234).split(dataset_x):
        print("Crossvalidation fold: {}".format(counter))
        for conf in configurations_dict:
            model, clls = get_arquitecture_dqn_cv(**conf, dataset_shape=dataset_x.shape)
            model.train(dataset_x[train_index],dataset_y[train_index], dataset_z[train_index], None, validation_data=(dataset_x[test_index], dataset_y[test_index]),num_qt_update=20,  batch_size=batch_size, epochs=epochs, verbose=0)
            conf['scores'].append(model.evaluate(dataset_x[test_index], dataset_y[test_index], dataset_z[test_index], verbose = 0))
        counter+=1
    for conf in configurations_dict:
        conf['avg'] = float(np.average(conf['scores']))
        conf['std'] = float(np.std(conf['scores']))
    print("Fin de crossvalidation")
    
    ord_configurations = sorted(configurations_dict, key=lambda el: el['avg'])
    data  = []
    for conf in ord_configurations:
        for score in conf['scores']:
            row = {}
            row['name'] = 'dqn'+conf['name']
            row['hidden_layers'] = conf['hidden_layers']
            row['units'] = conf['units']
            row['score'] = score
            row['avg'] = conf['avg']
            row['std'] = conf['std']
            data.append(row)

    pd.DataFrame(data).to_csv(url_to_save, index=False)
    print("resultado guardado en:"+url_to_save)
    print ('time: {}'.format(str(time.time() - start)))