from tensorflow.keras.models import load_model
from itertools import product
from sklearn.model_selection import KFold
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from tensorflow.keras import Sequential
from tensorflow.keras.layers import LSTM, Dense, Dropout, Activation
import time
from tensorflow import keras
import pandas as pd
import numpy as np

class DQNWrapper:

    def __init__(self, q_model, gamma, terminal_state = None):
        self.q_model = q_model
        self.q_model_target = keras.models.clone_model(q_model)
        self.__update_q_target()
        self.gamma = gamma
        self.terminal_state = terminal_state

    def train(self, X, Y, YS, TS, *, validation_data=None, num_qt_update = 100, batch_size = 1,epochs = 1000, callback_batch = lambda q_model, x, y, ys, counter : None , verbose=0):
        sub_epochs = int(np.floor(epochs/num_qt_update))
        residual_epochs = epochs%num_qt_update

        #new_y = self.__get_y_target_reward(X,Y, YS, TS)
        #return Y, new_y
        epoch_counter = 0
        sub_hist = None
        full_hist = []
        for _ in range(sub_epochs):
            new_y = self.__get_y_target_reward(X,Y, YS, TS)
            sub_hist = self.q_model.fit(x=X, y = new_y,  batch_size = batch_size, epochs=num_qt_update, verbose=0)
            self.__update_q_target()
            full_hist.append(sub_hist)
            epoch_counter+=num_qt_update
            callback_batch(self.q_model, X, Y, YS, epoch_counter)
        if(residual_epochs != 0):
            new_y = self.__get_y_target_reward(X,Y, YS, TS)
            sub_hist = self.q_model.fit(x=X, y = new_y,  batch_size = batch_size, epochs=residual_epochs, verbose=0)
            full_hist.append(sub_hist)
            epoch_counter+=residual_epochs
            callback_batch(self.q_model, X, Y, YS, epoch_counter)
        self.__update_q_target()
        self.last_history = full_hist
        return  self.last_history

    def evaluate(self, X, Y, Z, verbose=0):
        Y2 = self.__get_y_target_reward(X, Y, Z, None)
        return self.q_model.evaluate(X, Y2, verbose=0)
    def __get_y_target_reward(self, x, y, ys, ts):
        new_y = y+self.gamma*np.repeat(np.max(self.q_model.predict(ys, verbose=0), axis=1).reshape(ys.shape[0],1), 11, axis=1)
        #new_y[ts == True] = y[ts == True]
        return new_y


    def __update_q_target(self):
        self.q_model_target.set_weights(self.q_model.get_weights())