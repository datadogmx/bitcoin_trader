from collections import deque
import numpy as np
class SimpleMovingAverage():
	def __init__(self, past_steps, t_hold):
		self.past_steps = past_steps
		self.t_hold = t_hold
		self.stack = deque([],self.past_steps)

	def predict(self, value):
		self.stack.append(value)
		amount_spected = float(np.average(self.stack))
		decition = None
		if((amount_spected/value) > self.t_hold):
			decition = 1
		else:
			decition = 0
		return amount_spected, decition

