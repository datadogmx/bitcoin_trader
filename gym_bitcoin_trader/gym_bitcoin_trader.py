import os
import numpy as np
from IPython.display import clear_output
from IPython import get_ipython



def in_notebook():
    try:
        if 'IPKernelApp' not in get_ipython().config:
            return False
    except:
        return False
    return True

clear = lambda :  clear_output(True) if in_notebook() else os.system('clear')
class Drawer():
    width = None
    header = None
    def __update_header(self):
        len_title = int(np.floor((self.width - len(self.title))/2))
        res_title = (self.width - len(self.title))%2
        self.header = "+"+("+"*self.width)+"+\n"
        self.header += '+'+(" "*len_title)+self.title+(" "*(len_title+res_title))+"+\n"
        self.header += "+"+("+"*self.width)+"+\n"

    def __init__(self, title, width=50):
        self.width = width
        self.title = title
        
        self.__update_header()

    def print_status(self, last_value, current_value, date_time, wallet, last_reward, num_steps):
        clear()
        print(self.header)
        print("Fecha: {}".format(str(date_time)))
        print("Numero de pasos transcurridos: {}".format(num_steps))
        print("Precio anterior (USD): {}".format(last_value[0] if len(last_value) != 0 else "-"))
        print("Precio actual (USD): {}".format(current_value[0]))
        print("Cantidad en la bolsa: {} \t cantidad en bitcoins:{}".format(wallet['bag'],wallet['bitcoin']))

        print("Recompensa anterior: {}".format(last_reward))
        print("Ganancia en dinero acumulada: {}".format(wallet['bag']+wallet['bitcoin']))
        print("Recompensa acumulada: {}".format((wallet['bag']+wallet['bitcoin'])/1000))

class GymBitcoinTrader():

    step_num = None
    wallet = None
    status = 'full'
    name= 'Bitcoin trader game'
    relative_dataset = None
    last_step = None
    last_value = None
    last_reward = None
    last_bitcoin_change = None

    def __init__(self, full_dataset, full_dates, mode = 'test', history_l=0, partitions = [0.7,0.15,0.15]):
        """
            :param full_dataset: array
            :param full_dates: array
            :param mode: string ("test", "train")
            :param history_l: num of history to see before testing
            :param partitions: distribution partition for data

        """

        self.full_dataset = full_dataset
        self.full_dates = full_dates
        self.mode = mode
        self.history_l = history_l
        self.train_partition, self.test_partition, self.validation_partion = partitions
        self.START_TESTING = int(np.floor(full_dataset.shape[0]*(self.train_partition+self.test_partition)))

        self.__init_variables()
        print("Comenzando juego desde: {}".format(str(self.relative_dates[self.step_num])))


    def __get_total(self):
        return self.wallet['bag']+self.wallet['bitcoin']

    def __update_wallet(self, bag, bitcoin):
        self.wallet['bag'] = bag
        self.wallet['bitcoin'] = bitcoin

    def __isterminal(self):
        return self.step_num >= self.last_step

    def __init_variables(self):
        self.day = 0
        self.wallet = {
            "bag" :1000.0,
            "bitcoin": 0.0    
        }
        self.drawer = Drawer(self.name)
        start, end = 0, self.full_dataset.shape[0]
        if(self.mode == 'test'):
            start = self.START_TESTING - self.history_l
        elif(self.mode == 'train'):
            end = self.START_TESTING

        self.relative_dataset = self.full_dataset[start:end]
        self.last_value = []
        self.relative_dates = self.full_dates[start:end]
        self.last_step = self.relative_dataset.shape[0] - 1
        self.step_num = 0

    def __get_state(self):
        return self.relative_dataset[self.step_num], self.last_reward, self.__isterminal()

    def get_money(self):
        return self.__get_total()

    def render(self):
        self.drawer.print_status(self.last_value, self.relative_dataset[self.step_num], self.relative_dates[self.step_num], self.wallet, self.last_reward, self.step_num )

    def step(self, bag = 1, bitcoin = 0):
        if self.__isterminal():
            print("Juego terminado")
            return self.__get_state()
        total = self.__get_total()
        bag_t = total*bag
        bitcoin_t = (total-bag_t)

        self.last_value = self.relative_dataset[self.step_num]
        self.step_num+=1
        self.last_bitcoin_change =  self.relative_dataset[self.step_num][0]/self.last_value[0]

        bitcoin_t_n =bitcoin_t*self.last_bitcoin_change
        self.last_reward =  ((bag_t + bitcoin_t_n)/(bag_t+bitcoin_t) )
        self.__update_wallet(bag_t, bitcoin_t_n)
        return self.__get_state()

    def restart(self):
        self.__init_variables()